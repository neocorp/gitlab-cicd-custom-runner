FROM docker:dind

RUN echo 'http://dl-cdn.alpinelinux.org/alpine/latest-stable/main/' >> /etc/apk/repositories && \
    apk --no-cache update

# Install pip, poetry and tools
RUN apk --no-cache add python3=3.11.2-r0 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/main 
RUN apk add py3-pip=23.0.1-r0 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
RUN python -m pip install -U pip && python -m pip install poetry poetry-dynamic-versioning pylint

# Install Terraform
# RUN wget https://releases.hashicorp.com/terraform/0.12.25/terraform_0.12.25_linux_amd64.zip && \
#     mkdir terraform && \
#     unzip terraform_0.12.25_linux_amd64.zip -d terraform && \
#     cp terraform/terraform  /usr/local/bin/

# Install awscli
# RUN pip3 install awscli